@extends('layouts.mainstatic')
@section('content')
    <div ng-controller="inicioController">
        <div layout="row" layout-align="center center">
            <div flex="100" flex-gt-sm="50">
                <div class="centertext">
                    <img src="{{url('img/logo-big.svg')}}" style="width: 250px;padding-top: 36px;">
                </div>
                <br>
                <md-toolbar class="md-hue-2 secondarytoolbar">
                    <div class="md-toolbar-tools">
                        <h2 class="md-subhead">
                            <span>Iniciar Sesión</span>
                        </h2>
                        <span flex></span>
                    </div>
                </md-toolbar>
                <div layout="column" md-whiteframe="1" style="background-color: white;" layout-padding>
                	<form layout="row" layout-align="center center" ng-submit="iniciarSesion()">
                		<md-input-container flex="40">
                		    <label>E-Mail</label>
                		    <input type="text" name="email" ng-model="create.data.email" autocomplete="off">
                		</md-input-container>
                		<md-input-container flex="40">
                		    <label>Contraseña</label>
                		    <input type="password" name="password" ng-model="create.data.password" autocomplete="off">
                		</md-input-container>
            		    <div layout="row" layout-align="center center">
            		        <md-button class="md-primary md-raised" ng-disabled="create.loggingin == true" type="submit">Ingresar</md-button>
            		    </div>
                	</form>
                	<md-divider></md-divider>
                	<md-list>
                		<md-list-item class="md-2-line">
                		  <div class="md-list-item-text" layout="column">
                		    <h3>Autenticación</h3>
                		    <h4>Estado: 
                		    	<span ng-if="create.sesion === null" md-colors="{color:'accent-700'}">Pendiente</span>
                		    	<b ng-if="create.sesion === true" md-colors="{color:'primary'}">OK</b>
                		    	<b ng-if="create.sesion === false" md-colors="{color:'warn'}">Error</b>
                		    </h4>
                            <md-icon ng-show="create.loading.sesion" class="md-secondary" md-font-icon="fa fa-cog fa-spin fa-2x fa-fw"></md-icon>
                            <md-icon ng-if="!create.loading.sesion && create.sesion === true" class="md-secondary md-primary" md-font-icon="fa fa-check fa-2x"></md-icon>
                            <md-icon ng-if="!create.loading.sesion && create.sesion === false" class="md-secondary md-warn" md-font-icon="fa fa-times fa-2x"></md-icon>
                		  </div>
                		</md-list-item>
                		<md-list-item class="md-2-line">
                		  <div class="md-list-item-text" layout="column">
                		    <h3>Sincronización de Módulos</h3>
                		    <h4>Estado: 
                		    	<span ng-if="create.modulos === null" md-colors="{color:'accent-700'}">Pendiente</span>
                		    	<b ng-if="create.modulos === true" md-colors="{color:'primary'}">OK</b>
                		    	<b ng-if="create.modulos === false" md-colors="{color:'warn'}">Error</b>
                		    </h4>
                            <md-icon ng-if="create.loading.modulos" class="md-secondary" md-font-icon="fa fa-cog fa-spin fa-2x fa-fw"></md-icon>
                            <md-icon ng-if="!create.loading.modulos && create.modulos === true" class="md-secondary md-primary" md-font-icon="fa fa-check fa-2x"></md-icon>
                            <md-icon ng-if="!create.loading.modulos && create.modulos === false" class="md-secondary md-warn" md-font-icon="fa fa-times fa-2x"></md-icon>
                		  </div>
                		</md-list-item>
                        <md-list-item class="md-2-line">
                          <div class="md-list-item-text" layout="column">
                            <h3>Sincronización de Permisos</h3>
                            <h4>Estado: 
                                <span ng-if="create.permisos === null" md-colors="{color:'accent-700'}">Pendiente</span>
                                <b ng-if="create.permisos === true" md-colors="{color:'primary'}">OK</b>
                                <b ng-if="create.permisos === false" md-colors="{color:'warn'}">Error</b>
                            </h4>
                            <md-icon ng-if="create.loading.permisos" class="md-secondary" md-font-icon="fa fa-cog fa-spin fa-2x fa-fw"></md-icon>
                            <md-icon ng-if="!create.loading.permisos && create.permisos === true" class="md-secondary md-primary" md-font-icon="fa fa-check fa-2x"></md-icon>
                            <md-icon ng-if="!create.loading.permisos && create.permisos === false" class="md-secondary md-warn" md-font-icon="fa fa-times fa-2x"></md-icon>
                          </div>
                        </md-list-item>
                        <md-list-item class="md-2-line">
                          <div class="md-list-item-text" layout="column">
                            <h3>Información Regional</h3>
                            <h4>Estado: 
                                <span ng-if="create.regional === null" md-colors="{color:'accent-700'}">Pendiente</span>
                                <b ng-if="create.regional === true" md-colors="{color:'primary'}">OK</b>
                                <b ng-if="create.regional === false" md-colors="{color:'warn'}">Error</b>
                            </h4>
                            <md-icon ng-if="create.loading.regional" class="md-secondary" md-font-icon="fa fa-cog fa-spin fa-2x fa-fw"></md-icon>
                            <md-icon ng-if="!create.loading.regional && create.regional === true" class="md-secondary md-primary" md-font-icon="fa fa-check fa-2x"></md-icon>
                            <md-icon ng-if="!create.loading.regional && create.regional === false" class="md-secondary md-warn" md-font-icon="fa fa-times fa-2x"></md-icon>
                          </div>
                        </md-list-item>
                		<md-list-item class="md-2-line">
                		  <div class="md-list-item-text" layout="column">
                		    <h3>Información Tributaria</h3>
                		    <h4>Estado: 
                		    	<span ng-if="create.tributario === null" md-colors="{color:'accent-700'}">Pendiente</span>
                		    	<b ng-if="create.tributario === true" md-colors="{color:'primary'}">OK</b>
                		    	<b ng-if="create.tributario === false" md-colors="{color:'warn'}">Error</b>
                		    </h4>
                            <md-icon ng-if="create.loading.tributario" class="md-secondary" md-font-icon="fa fa-cog fa-spin fa-2x fa-fw"></md-icon>
                            <md-icon ng-if="!create.loading.tributario && create.tributario === true" class="md-secondary md-primary" md-font-icon="fa fa-check fa-2x"></md-icon>
                            <md-icon ng-if="!create.loading.tributario && create.tributario === false" class="md-secondary md-warn" md-font-icon="fa fa-times fa-2x"></md-icon>
                		  </div>
                		</md-list-item>
                		<md-list-item class="md-2-line">
                		  <div class="md-list-item-text" layout="column">
                		    <h3>Descarga de Productos</h3>
                		    <h4>Estado: 
                		    	<span ng-if="create.productos === null" md-colors="{color:'accent-700'}">Pendiente</span>
                		    	<b ng-if="create.productos === true" md-colors="{color:'primary'}">OK</b>
                		    	<b ng-if="create.productos === false" md-colors="{color:'warn'}">Error</b>
                		    </h4>
                            <md-icon ng-if="create.loading.productos" class="md-secondary" md-font-icon="fa fa-cog fa-spin fa-2x fa-fw"></md-icon>
                            <md-icon ng-if="!create.loading.productos && create.productos === true" class="md-secondary md-primary" md-font-icon="fa fa-check fa-2x"></md-icon>
                            <md-icon ng-if="!create.loading.productos && create.productos === false" class="md-secondary md-warn" md-font-icon="fa fa-times fa-2x"></md-icon>
                		  </div>
                		</md-list-item>
                	</md-list>
                </div>
            </div>
        </div>
        <span></span>
    </div>
@endsection