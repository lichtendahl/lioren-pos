@extends('layouts.mainstatic')
@section('content')
    <div>
        <div layout="row" layout-align="center center">
            <div flex="100" flex-gt-sm="50">
                <div class="centertext">
                    <img src="{{url('img/logo-big.svg')}}" style="width: 250px;padding-top: 36px;">
                </div>
                <br>
                <md-toolbar class="md-hue-2 secondarytoolbar">
                    <div class="md-toolbar-tools">
                        <h2 class="md-subhead">
                            <span>Configurar POS</span>
                        </h2>
                        <span flex></span>
                    </div>
                </md-toolbar>
                <div layout="column" md-whiteframe="1" style="background-color: white;" layout-padding>
                    <div layout="column" layout-padding>
                        <h3 class="md-title">Registrar Punto de Ventas</h3>
                        <p>Ingresa el <em md-colors="{color:'primary'}">Secreto POS</em> de este Punto de Ventas en la Plataforma Web.</p>
                        <md-input-container>
                            <label>Secreto</label>
                            <md-icon md-font-icon="fa-key" class="fa"></md-icon>
                            <input ng-model="uid" type="text" ng-init="uid='{{$uid}}'" ng-readonly="true">
                        </md-input-container>
                    </div>
                    <div layout="row" layout-align="center center">
                        <md-button class="md-primary" href="/">
                            Regresar
                        </md-button>
                    </div>
                </div>
            </div>
        </div>
        <span></span>
    </div>
@endsection