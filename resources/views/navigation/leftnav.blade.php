<md-sidenav class="md-sidenav-left"
			md-component-id="left"
			md-is-locked-open="$mdMedia('gt-md')"
			md-whiteframe="4">
	<md-toolbar class="md-theme-indigo">
		<div class="md-toolbar-tools" layout="row" layout-align="space-between center">
			<div layout="column">
				<h1 class="md-toolbar-tools">Panel lateral</h1>
			</div>
			<md-button class="md-icon-button" aria-label="More">
			  <md-icon md-font-icon="fa-cog" class="fa"></md-icon>
			</md-button>
		</div>
	</md-toolbar>
	<md-content ng-controller="leftNavController" 
				class="leftNavContainer"
				>
	</md-content>
</md-sidenav>