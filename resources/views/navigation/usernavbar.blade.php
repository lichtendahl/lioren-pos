<md-content>
	<md-nav-bar md-selected-nav-item="currentNavItem" nav-bar-aria-label="navigation links" layout-align="space-between center">
		<div flex>
			<md-nav-item md-nav-click="goto('dashboard')" name="dashboard" class="md-primary">{{$config->fantasia}}</md-nav-item>
		</div>
		<div flex layout="row">
			<md-nav-item md-nav-click="goto('caja')" name="caja">Caja</md-nav-item>
			<md-nav-item md-nav-click="goto('ventas')" name="ventas">Ventas</md-nav-item>
			<md-nav-item md-nav-click="goto('productos')" name="productos">Productos</md-nav-item>
		</div>
		<div flex layout="column" layout-align="center end">
			<md-menu flex>
			  <md-button aria-label="Open phone interactions menu" ng-click="openMenu($mdOpenMenu, $event)" class="md-primary">
			    <md-icon md-menu-origin md-font-icon="fa-user-circle-o" class="fa"></md-icon>
			    {{$user->name}}
			  </md-button>
			  <md-menu-content width="4">
			    <md-menu-item>
			      <md-button ng-href="/abrircaja">
			        <md-icon md-font-icon="fa-unlock-alt" class="fa"></md-icon>
			        Abrir la gaveta
			      </md-button>
			    </md-menu-item>
			    <md-menu-divider></md-menu-divider>
			    <md-menu-item>
			      <md-button ng-href="/logout">
			        <md-icon md-font-icon="fa-sign-out" class="fa"></md-icon>
			        Cerrar Sesión
			      </md-button>
			    </md-menu-item>
			  </md-menu-content>
			</md-menu>
		</div>
		<div flex hide>
			<md-button class="md-icon-button md-accent" aria-label="Settings" ng-click="null" style="margin:5px 0 0 5px;max-width: 40px">
			  <md-icon>
			  	<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 18 18">
			  	    <path d="M0 0h18v18h-18z" fill="none"/>
			  	    <path d="M2 13.5h14v-1.5h-14v1.5zm0-4h14v-1.5h-14v1.5zm0-5.5v1.5h14v-1.5h-14z"/>
			  	</svg>
			  </md-icon>
			</md-button>
		</div>
	</md-nav-bar>
</md-content>