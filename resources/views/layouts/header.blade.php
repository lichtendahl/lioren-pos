<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#673AB7" />

    <title>Punto de Venta</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{url('img/favicon.png')}}" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,400italic">

    <!-- Angular Material CSS -->
    <link rel="stylesheet" href="{{url('css/angular-material.min.css')}}">

    <!-- Angular -->
    <script type="text/javascript" src="{{url('js/angular.js')}}"></script>
    <script type="text/javascript" src="{{url('js/angular-animate.js')}}"></script>
    <script type="text/javascript" src="{{url('js/angular-locale_es-cl.js')}}"></script>

    <!-- Angular Material -->
    <script src="{{url('js/angular-aria.min.js')}}"></script>
    <script src="{{url('js/angular-messages.min.js')}}"></script>
    <script src="{{url('js/angular-material.min.js')}}"></script>

    <!-- Angular app -->
    <script type="text/javascript" src="{{url('js/ng-app.js')}}"></script>

    <script type="text/javascript" src="{{url('js/controllers/setupController.js')}}"></script>

    <!-- Font-Awesome -->
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">

    <!-- Custom styles -->
    <link href="{{ url('css/style.css') }}" rel="stylesheet">

</head>