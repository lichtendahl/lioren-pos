<!DOCTYPE html>
<html lang="en" ng-app="pos">
@include('layouts.header')
<body style="overflow:hidden;" layout="column" ng-controller="mainController">
	@include('common.errors')
	<md-content style="height: 100vh;background-image: url('/img/pbg2.jpg');background-size: cover" layout-align="space-between stretch" layout="column">
		@yield('content')
	</md-content>
</body>
</html>