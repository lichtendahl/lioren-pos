<!DOCTYPE html>
<html lang="en" ng-app="pos">
@include('layouts.header')
<body 	ng-controller="mainController">
	@include('navigation.usernavbar')
	<section layout="row">
		<md-content flex layout="column" class="content">
			<div class="ext-content" ng-include="mainContent" flex onload="loadingContent=false">
				External content for <span>@{{mainContent}}</span>
			</div>
		</md-content>
	</section>
	@include('common.errors')
	@include('layouts.footer')
</body>
</html>