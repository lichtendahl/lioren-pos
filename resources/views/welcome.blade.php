@extends('layouts.mainstatic')
@section('content')
    <div flex layout="column" layout-align="space-between stretch" ng-controller="setupController">
        <div layout="row" layout-padding>
            <span flex></span>
            <div class="links">
                <a href="{{ url('/setup') }}">Configurar</a>
            </div>
        </div>


        <div layout="row" layout-align="center center">
            <div layout="column">
                @if($config != NULL && $config->logo != NULL)
                <div style="text-align:center">
                    <img src="data:image/jpeg;base64,{!!$config->logo!!}" alt="logo" />
                </div>
                @else
                <div class="splashlogo" style="margin:auto"><img src="{{url('img/logo-mini.svg')}}"></div>
                @endif
                <div class="title" style="font-weight: 100">
                    Punto de Ventas
                </div>
                <div class="links" layout="row" layout-align="center center">
                    @if($config != NULL && $config->printer != NULL)
                    <a href="{{url('inicio')}}">Iniciar Sesión</a>
                    @elseif($config != NULL && $config->printer == NULL)
                    <p md-colors="{color:'warn'}">Configuración incompleta</p>
                    @endif
                </div>
            </div>
        </div>
        <div></div>
        <div></div>
    </div>
@endsection