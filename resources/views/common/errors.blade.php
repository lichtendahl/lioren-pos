@if (count($errors) > 0 || (session()->has('messages') && count(session('messages')) > 0))
    <!-- Form Error List -->
    <div style="visibility: hidden;">
      <div class="md-dialog-container" id="commonErrors" ng-init="showPrerenderedDialog($event)">
        <md-dialog layout-padding>
          @if (count($errors) > 0)
            <h3><i class="fa fa-exclamation-triangle" aria-hidden="true" md-colors="{color:'warn'}"></i> No se ha podido procesar la solicitud</h3>
            @foreach ($errors->all() as $error)
                <p>{!! $error !!}</p>
            @endforeach
          @endif
          @if (session()->has('messages') && count(session('messages')) > 0)
            <h3><i class="fa fa-check-circle-o" aria-hidden="true" md-colors="{color:'primary'}"></i> La acción se ha ejecutado correctamente</h3>
            @foreach (session('messages') as $message)
                <p>{!! $message !!}</p>
            @endforeach
          @endif
        </md-dialog>
      </div>
    </div>
@endif