<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/logout', 'LoginController@logout');
Route::get('/abrircaja', 'LoginController@abrirCaja');
Route::get('/testprint', 'LoginController@testPrint');
Route::get('printtest', ['uses' => 'PrinterController@test']);
Route::post('printarray', ['uses' => 'PrinterController@run']);

# CONFIG
Route::get('setup', 'SetupController@index');
Route::get('setup/1', ['uses' => 'SetupController@inicio', 'as' => 'setup.inicio']);

Route::get('/home', 'HomeController@index');
