var app = angular.module('pos', ['ngMessages', 'ngAnimate', 'ngMaterial']);

app.config(function($mdDateLocaleProvider) {
	$mdDateLocaleProvider.formatDate = function(date) {
		return moment(date).format('DD/MM/YYYY');
	};
});

app.config(function($mdThemingProvider) {
		$mdThemingProvider.theme('default')
			.primaryPalette('teal')
			.accentPalette('orange')
			.warnPalette('red');
});

app.controller('mainController', function($scope, $mdSidenav, $timeout, $location, $mdDialog, $http, $mdDialog, $element, $window, $mdToast, $mdComponentRegistry) {

	$scope.showSimpleToast = function($content) {
	  $mdToast.show(
	    $mdToast.simple()
	      .textContent($content)
	      .position('top right')
	      .hideDelay(2000)
	  );
	};

	$scope.delayedOpenRight = function () {
		$mdComponentRegistry.when('right').then(function() {
			$timeout(function(){
				$mdSidenav('right').open()
				.then(function () {

				});
			}, 100)
		});
	};

	$scope.closeRight = function () {
		$mdComponentRegistry.when('right').then(function() {
			$mdSidenav('right').close()
			  .then(function () {
			  });
		});
	};

	$scope.myDate = new Date();

	$scope.minDate = new Date(
	    $scope.myDate.getFullYear() - 100,
	    $scope.myDate.getMonth(),
	    $scope.myDate.getDate());

	$scope.maxDate = new Date(
	    $scope.myDate.getFullYear() - 3,
	    $scope.myDate.getMonth(),
	    $scope.myDate.getDate());

	$scope.currentNavItem = 'dashboard';

	// Inicializacion contenido
	if($location.path() == '')
	{
		$scope.mainContent = 'dashboard';
	}
	else
	{
		$scope.mainContent = $location.path();
		$scope.currentNavItem = $location.path().substring(1);
	}

	$scope.goto = function($page) {
		$scope.mainContent = $page;
		$location.path($page);
	};


	$scope.toggleLeft = buildDelayedToggler('left');
	$scope.toggleRight = buildToggler('right');
	$scope.isOpenRight = function(){
	  return $mdSidenav('right').isOpen();
	};

	/**
	 * Supplies a function that will continue to operate until the
	 * time is up.
	 */
	function debounce(func, wait, context) {
	  var timer;

	  return function debounced() {
	    var context = $scope,
	        args = Array.prototype.slice.call(arguments);
	    $timeout.cancel(timer);
	    timer = $timeout(function() {
	      timer = undefined;
	      func.apply(context, args);
	    }, wait || 10);
	  };
	}

	/**
	 * Build handler to open/close a SideNav; when animation finishes
	 * report completion in console
	 */
	function buildDelayedToggler(navID) {
	  return debounce(function() {
	    // Component lookup should always be available since we are not using `ng-if`
	    $mdSidenav(navID)
	      .toggle()
	      .then(function () {
	      });
	  }, 200);
	}

	function buildToggler(navID) {
	  return function() {
	    // Component lookup should always be available since we are not using `ng-if`
	    $mdSidenav(navID)
	      .toggle()
	      .then(function () {
	      });
	  }
	}

		var originatorEv;

	    $scope.openMenu = function($mdOpenMenu, ev) {
	      originatorEv = ev;
	      $mdOpenMenu(ev);
	    };

	    $scope.showPrerenderedDialog = function(ev) {
	      $mdDialog.show({
	        controller: DialogController,
	        contentElement: '#commonErrors',
	        parent: angular.element(document.body),
	        targetEvent: ev,
	        clickOutsideToClose: true,
	        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
	      });
	    };

	    function DialogController($scope, $mdDialog) {
	      $scope.hide = function() {
	        $mdDialog.hide();
	      };

	      $scope.cancel = function() {
	        $mdDialog.cancel();
	      };

	      $scope.answer = function(answer) {
	        $mdDialog.hide(answer);
	      };
	    };

	    $scope.showAlert = function(title, content, label, ok) {
	      // Appending dialog to document.body to cover sidenav in docs app
	      // Modal dialogs should fully cover application
	      // to prevent interaction outside of dialog
	      $mdDialog.show(
	        $mdDialog.alert()
	          .parent(angular.element(document.querySelector('#popupContainer')))
	          .clickOutsideToClose(true)
	          .title(title)
	          .textContent(content)
	          .ariaLabel(label)
	          .ok(ok)
	      );
	    };


	    // PRODUCTOS

	    $scope.productos = {
	    	busqueda: {
	    		nombre: '',
	    		codigo: '',
	    		param1: '',
	    		param2: '',
	    		param3: '',
	    	},
	    	sort: {
	    		orderby: 'codigo',
	    		direction: 'asc'
	    	},
	    	busquedaActiva: {
	    		nombre: '',
	    		codigo: '',
	    		param1: '',
	    		param2: '',
	    		param3: '',
	    	},
	    	pagination: {
	    		data: []
	    	},
	    };

	    $scope.buscarProductos = function()
	    {
	    	$http({
	    	  method: 'GET',
	    	  url: 	'/ajax/productos/buscarproductos'+
	    	  		'?nombre='+$scope.productos.busqueda.nombre+
	    	  		'&codigo='+$scope.productos.busqueda.codigo+
	    	  		'&param1='+$scope.productos.busqueda.param1+
	    	  		'&param2='+$scope.productos.busqueda.param2+
	    	  		'&param3='+$scope.productos.busqueda.param3+
	    	  		'&orderby='+$scope.productos.sort.orderby+
	    	  		'&direction='+$scope.productos.sort.direction,
	    	}).then(function successCallback(response) {
	    		if(response.data == false)
	    		{
	    			$scope.productos.pagination = {data: []};
	    		}
	    		else
	    		{
	    			$scope.productos.busquedaActiva = $scope.productos.busqueda;
	    			$scope.productos.pagination = response.data;
	    		}
	    	    // this callback will be called asynchronously
	    	    // when the response is available
	    	  }, function errorCallback(response) {
	    	    // called asynchronously if an error occurs
	    	    // or server returns response with an error status.
	    	  });
	    };

	    $scope.buscarProductosPrev = function()
	    {
	    	$http({
	    	  method: 'GET',
	    	  url: 	$scope.productos.pagination.prev_page_url+
	    	  		'&nombre='+$scope.productos.busquedaActiva.nombre+
	    	  		'&codigo='+$scope.productos.busquedaActiva.codigo+
	    	  		'&param1='+$scope.productos.busquedaActiva.param1+
	    	  		'&param2='+$scope.productos.busquedaActiva.param2+
	    	  		'&param3='+$scope.productos.busquedaActiva.param3+
	    	  		'&orderby='+$scope.productos.sort.orderby+
	    	  		'&direction='+$scope.productos.sort.direction,
	    	}).then(function successCallback(response) {
	    		if(response.data == false)
	    		{
	    			$scope.productos.pagination = {data: []};
	    		}
	    		else
	    		{
	    			$scope.productos.pagination = response.data;
	    		}
	    	    // this callback will be called asynchronously
	    	    // when the response is available
	    	  }, function errorCallback(response) {
	    	    // called asynchronously if an error occurs
	    	    // or server returns response with an error status.
	    	  });
	    };

	    $scope.buscarProductosNext = function()
	    {
	    	$http({
	    	  method: 'GET',
	    	  url: 	$scope.productos.pagination.next_page_url+
	    	  		'&nombre='+$scope.productos.busquedaActiva.nombre+
	    	  		'&codigo='+$scope.productos.busquedaActiva.codigo+
	    	  		'&param1='+$scope.productos.busquedaActiva.param1+
	    	  		'&param2='+$scope.productos.busquedaActiva.param2+
	    	  		'&param3='+$scope.productos.busquedaActiva.param3+
	    	  		'&orderby='+$scope.productos.sort.orderby+
	    	  		'&direction='+$scope.productos.sort.direction,
	    	}).then(function successCallback(response) {
	    		if(response.data == false)
	    		{
	    			$scope.productos.pagination = {data: []};
	    		}
	    		else
	    		{
	    			$scope.productos.pagination = response.data;
	    		}
	    	    // this callback will be called asynchronously
	    	    // when the response is available
	    	  }, function errorCallback(response) {
	    	    // called asynchronously if an error occurs
	    	    // or server returns response with an error status.
	    	  });
	    };

	    $scope.buscarProductosSort = function(orderby, direction)
	    {
	    	$scope.productos.sort = {
	    		orderby: orderby,
	    		direction: direction
	    	};
	    	console.log('/ajax/productos/buscarproductos'+
	    	  		'?nombre='+$scope.productos.busquedaActiva.nombre+
	    	  		'&codigo='+$scope.productos.busquedaActiva.codigo+
	    	  		'&param1='+$scope.productos.busquedaActiva.param1+
	    	  		'&param2='+$scope.productos.busquedaActiva.param2+
	    	  		'&param3='+$scope.productos.busquedaActiva.param3+
	    	  		'&orderby='+$scope.productos.sort.orderby+
	    	  		'&direction='+$scope.productos.sort.direction);
	    	$http({
	    	  method: 'GET',
	    	  url: 	'/ajax/productos/buscarproductos'+
	    	  		'?nombre='+$scope.productos.busquedaActiva.nombre+
	    	  		'&codigo='+$scope.productos.busquedaActiva.codigo+
	    	  		'&param1='+$scope.productos.busquedaActiva.param1+
	    	  		'&param2='+$scope.productos.busquedaActiva.param2+
	    	  		'&param3='+$scope.productos.busquedaActiva.param3+
	    	  		'&orderby='+$scope.productos.sort.orderby+
	    	  		'&direction='+$scope.productos.sort.direction,
	    	}).then(function successCallback(response) {
	    		console.log(response.data);
	    		if(response.data == false)
	    		{
	    			$scope.productos.pagination = {data: []};
	    		}
	    		else
	    		{
	    			$scope.productos.pagination = response.data;
	    		}
	    	    // this callback will be called asynchronously
	    	    // when the response is available
	    	  }, function errorCallback(response) {
	    	    // called asynchronously if an error occurs
	    	    // or server returns response with an error status.
	    	  });
	    };

	    // CAJA

	    $scope.showResumenApertura = false;

	    $scope.apertura = {
	    	observaciones: '',
	    	monedas: {
	    		am1: {valor: 1, monto: 0, cantidad: 0},
	    		am5: {valor: 5, monto: 0, cantidad: 0},
	    		am10: {valor: 10, monto: 0, cantidad: 0},
	    		am50: {valor: 50, monto: 0, cantidad: 0},
	    		am100: {valor: 100, monto: 0, cantidad: 0},
	    		am500: {valor: 500, monto: 0, cantidad: 0},
	    	},
	    	billetes: {
	    		ab500: {valor: 500, monto: 0, cantidad: 0},
	    		ab1000: {valor: 1000, monto: 0, cantidad: 0},
	    		ab2000: {valor: 2000, monto: 0, cantidad: 0},
	    		ab5000: {valor: 5000, monto: 0, cantidad: 0},
	    		ab10000: {valor: 10000, monto: 0, cantidad: 0},
	    		ab20000: {valor: 20000, monto: 0, cantidad: 0},
	    	},
	    };

	    $scope.showResumenCierre = false;

	    $scope.cierre = {
	    	observaciones: '',
	    	monedas: {
	    		cm1: {valor: 1, monto: 0, cantidad: 0},
	    		cm5: {valor: 5, monto: 0, cantidad: 0},
	    		cm10: {valor: 10, monto: 0, cantidad: 0},
	    		cm50: {valor: 50, monto: 0, cantidad: 0},
	    		cm100: {valor: 100, monto: 0, cantidad: 0},
	    		cm500: {valor: 500, monto: 0, cantidad: 0},
	    	},
	    	billetes: {
	    		cb500: {valor: 500, monto: 0, cantidad: 0},
	    		cb1000: {valor: 1000, monto: 0, cantidad: 0},
	    		cb2000: {valor: 2000, monto: 0, cantidad: 0},
	    		cb5000: {valor: 5000, monto: 0, cantidad: 0},
	    		cb10000: {valor: 10000, monto: 0, cantidad: 0},
	    		cb20000: {valor: 20000, monto: 0, cantidad: 0},
	    	},
	    };

	    $scope.calcularCantidad = function(type, key){
	    	if(type == 'monedas')
	    	{
	    		$scope.apertura.monedas[key].cantidad = $scope.apertura.monedas[key].monto / $scope.apertura.monedas[key].valor;
	    	}
	    	else if(type == 'billetes')
	    	{
	    		$scope.apertura.billetes[key].cantidad = $scope.apertura.billetes[key].monto / $scope.apertura.billetes[key].valor;
	    	}
	    };

	    $scope.calcularCantidadCierre = function(type, key){
	    	if(type == 'monedas')
	    	{
	    		$scope.cierre.monedas[key].cantidad = $scope.cierre.monedas[key].monto / $scope.cierre.monedas[key].valor;
	    	}
	    	else if(type == 'billetes')
	    	{
	    		$scope.cierre.billetes[key].cantidad = $scope.cierre.billetes[key].monto / $scope.cierre.billetes[key].valor;
	    	}
	    };

	    $scope.totalApertura = function(){
	    	var totalApertura = 0;
	    	for (var i = $scope.apertura.monedas.length - 1; i >= 0; i--) {
	    		totalApertura += $scope.apertura.monedas[i].monto;
	    	}
	    	for (var i = $scope.apertura.billetes.length - 1; i >= 0; i--) {
	    		totalApertura += $scope.apertura.billetes[i].monto;
	    	}

	    	return totalApertura;
	    };

	    $scope.totalCierre = function(){
	    	var totalCierre = 0;
	    	for (var i = $scope.cierre.monedas.length - 1; i >= 0; i--) {
	    		totalApertura += $scope.cierre.monedas[i].monto;
	    	}
	    	for (var i = $scope.cierre.billetes.length - 1; i >= 0; i--) {
	    		totalApertura += $scope.cierre.billetes[i].monto;
	    	}

	    	return totalCierre;
	    };

	    $scope.validarEnterosApertura = function(){
	    	for (var i = $scope.apertura.monedas.length - 1; i >= 0; i--) {
	    		if($scope.apertura.monedas[i].monto % $scope.apertura.monedas[i].valor != 0)
	    		{
	    			return false;
	    		}
	    	}
	    	for (var i = $scope.apertura.billetes.length - 1; i >= 0; i--) {
	    		if($scope.apertura.billetes[i].monto % $scope.apertura.billetes[i].valor != 0)
	    		{
	    			return false;
	    		}
	    	}
	    	return true;
	    };

	    $scope.validarEnterosCierre = function(){
	    	for (var i = $scope.cierre.monedas.length - 1; i >= 0; i--) {
	    		if($scope.cierre.monedas[i].monto % $scope.cierre.monedas[i].valor != 0)
	    		{
	    			return false;
	    		}
	    	}
	    	for (var i = $scope.cierre.billetes.length - 1; i >= 0; i--) {
	    		if($scope.cierre.billetes[i].monto % $scope.cierre.billetes[i].valor != 0)
	    		{
	    			return false;
	    		}
	    	}
	    	return true;
	    };

	    $scope.logThis = function($logo)
	    {
	    	console.log($logo);
	    }

	    // CLIENTES

	    $scope.currentGroup = null;

	    $scope.setCurrentGroup = function(key)
	    {
	    	$scope.currentGroup = $scope.clientes.grupos[key];
	    };

	    $scope.clientes = {
	    	personas: {
	    		busqueda: {rut: ''},
	    		data: {

	    		},
	    		datos: {

	    		},
	    	},
	    	empresas: {
	    		busqueda: '',
	    		data: {

	    		},
	    	},
	    	grupos: {},
	    };

	    $scope.showInputCliente = false;
	    $scope.showInfoCliente = false;

	    $scope.buscarRutPersona = function()
	    {
	    	console.log('buscando ' + $scope.clientes.personas.busqueda + ' en ' + '/ajax/clientes/buscarpersona')
	    	$http({
	    	  method: 'POST',
	    	  url: '/ajax/clientes/buscarpersona',
	    	  data: $scope.clientes.personas.busqueda
	    	}).then(function successCallback(response) {
	    		console.log(response.data);
	    		if(response.data == false)
	    		{
	    			console.log('showing input');
	    			$scope.showInputCliente = true;
	    			$scope.showInfoCliente = false;
	    			$scope.clientes.personas.data.rut = $scope.clientes.personas.busqueda.rut;
	    		}
	    		else
	    		{
	    			console.log('showing info');
	    			$scope.showInputCliente = false;
	    			$scope.showInfoCliente = true;
	    			$scope.clientes.personas.datos = response.data;
	    		}
	    	    // this callback will be called asynchronously
	    	    // when the response is available
	    	  }, function errorCallback(response) {
	    	    // called asynchronously if an error occurs
	    	    // or server returns response with an error status.
	    	  });
	    };

	    // VENTAS

	    $scope.mediosdepago = {
	    	0: {
	    		nombre: 'Efectivo',
	    		activo: true,
	    		icon: 'fa-money',
	    	},
	    	1: {
	    		nombre: 'Tarjeta de Débito',
	    		activo: true,
	    		icon: 'fa-credit-card-alt',
	    	},
	    	2: {
	    		nombre: 'Tarjeta de Crédito',
	    		activo: true,
	    		icon: 'fa-credit-card',
	    	},
	    	3: {
	    		nombre: 'Convenio',
	    		activo: false,
	    		icon: 'fa-list-alt',
	    	}
	    }

	    $scope.ventas = {
	    	showInfoPersona: false,
	    	personas: {
	    		data: {

	    		},
	    	},
	    	busqueda: {
	    		barcode: '',
	    	},
	    	data: {
	    		productos: [],
	    		cliente: {
	    			porcentajedescuento: 0,
	    		},
	    		mediodepago: 0,
	    	},
	    };

	    $scope.setFocus = function(id) {
			var element = $window.document.getElementById(id);
			element.focus();
	    };

	    $scope.buscarBarcode = function()
	    {
	    	$http({
	    	  method: 'POST',
	    	  url: '/ajax/ventas/buscarbarcode',
	    	  data: $scope.ventas.busqueda
	    	}).then(function successCallback(response) {
	    		console.log(response.data);
	    		if(response.data == false)
	    		{
	    			$scope.ventas.busqueda.barcode = '';
	    			$scope.showAlert('Producto no encontrado', 'El codigo especificado no está registrado en el sistema.', 'producto no encontrado', 'Cerrar');
	    			$scope.setFocus('inputbarcodeventas');
	    			// BARCODE no encontrado
	    		}
	    		else
	    		{
	    			var added = false;
	    			for (var i = $scope.ventas.data.productos.length - 1; i >= 0; i--) {
	    				if($scope.ventas.data.productos[i].producto.codigo == response.data.producto.codigo)
	    				{
	    					$scope.ventas.data.productos[i].cantidad += response.data.cantidad;
	    					if($scope.ventas.data.productos[i].cantidad <= 0)
	    					{
	    						$scope.eliminarVentaProducto(i);
	    					}
	    					added = true;
	    					break;
	    				}
	    			}
	    			if(added == false)
	    			{
	    				$scope.ventas.data.productos.push(response.data);
	    			}
	    			$scope.ventas.busqueda.barcode = '';
	    			$scope.setFocus('inputbarcodeventas');
	    		}
	    	    // this callback will be called asynchronously
	    	    // when the response is available
	    	  }, function errorCallback(response) {
	    	    // called asynchronously if an error occurs
	    	    // or server returns response with an error status.
	    	  });
	    };

	    $scope.eliminarVentaProducto = function(key)
	    {
	    	$scope.ventas.data.productos.splice(key,1);
	    };

	    $scope.getSubtotal = function()
	    {
	    	var $subtotal = 0;
	    	for (var i = $scope.ventas.data.productos.length - 1; i >= 0; i--) {
	    		$subtotal += $scope.ventas.data.productos[i].cantidad * ($scope.ventas.data.productos[i].producto.precio - $scope.ventas.data.productos[i].producto.precio * $scope.ventas.data.productos[i].producto.porcentajedescuento/100);
	    	}

	    	return $subtotal;
	    }

	    $scope.getDescuento = function()
	    {
    		return Math.round($scope.ventas.data.cliente.porcentajedescuento/100 * $scope.getSubtotal());
	    }

	    $scope.getTotal = function()
	    {
	    	return $scope.getSubtotal() - $scope.getDescuento();
	    }

	    $scope.buscarRutVenta = function() {
	    	console.log('buscando ' + $scope.clientes.personas.busqueda + ' en ' + '/ajax/clientes/buscarpersona')
	    	$http({
	    	  method: 'POST',
	    	  url: '/ajax/clientes/buscarpersona',
	    	  data: $scope.clientes.personas.busqueda
	    	}).then(function successCallback(response) {
	    		console.log(response.data);
	    		if(response.data == false)
	    		{
	    			console.log('showing input');
	    			$scope.showAdvanced();
	    		}
	    		else
	    		{
	    			if(response.data.convenio)
	    			{
	    				$scope.mediosdepago[3].activo = true;
	    			}
	    			$scope.ventas.data.cliente = response.data;
	    			$scope.ventas.showInfoPersona = true;

	    			console.log('showing info');
	    		}
	    	    // this callback will be called asynchronously
	    	    // when the response is available
	    	  }, function errorCallback(response) {
	    	    // called asynchronously if an error occurs
	    	    // or server returns response with an error status.
	    	  });
	    }

	    $scope.showAdvanced = function(ev) {
	      $mdDialog.show({
	        controller: AdvancedDialogController,
	        templateUrl: 'inputClientePrompt.html',
	        parent: angular.element(document.body),
	        targetEvent: ev,
	        locals : {
	            rut : $scope.clientes.personas.busqueda.rut
	        },
	        clickOutsideToClose:false,
	        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
	      })
	      .then(function(answer) {
	        $scope.status = 'You said the information was "' + answer + '".';
	        $scope.buscarRutVenta();
	      }, function() {
	        $scope.status = 'You cancelled the dialog.';
	      });
	    };

	    function AdvancedDialogController($scope, $mdDialog, rut) {
	    	$scope.ventas = {
	    		personas: {
	    			data: {
	    				rut: rut,
	    			},
	    		},
	    	};

	    	$scope.myDate = new Date();

	    	$scope.minDate = new Date(
	    	    $scope.myDate.getFullYear() - 100,
	    	    $scope.myDate.getMonth(),
	    	    $scope.myDate.getDate());

	    	$scope.maxDate = new Date(
	    	    $scope.myDate.getFullYear() - 3,
	    	    $scope.myDate.getMonth(),
	    	    $scope.myDate.getDate());

	      $scope.cancel = function() {
	        $mdDialog.cancel();
	      };

	      $scope.answer = function(answer) {
	        $mdDialog.hide(answer);
	      };

	      $scope.guardarPersona = function()
	      {
	      	$http({
	      	  method: 'POST',
	      	  url: '/ajax/ventas/guardarpersona',
	      	  data: $scope.ventas.personas.data
	      	}).then(function successCallback(response) {
	      		console.log(response.data);
	      		if(response.data == true)
	      		{
	      			console.log('Persona guardada OK');
	      			$scope.answer('OK');
	      		}
	      		else
	      		{
	      			console.log('Persona guardada FAIL');
	      		}
	      	    // this callback will be called asynchronously
	      	    // when the response is available
	      	  }, function errorCallback(response) {
	      	    // called asynchronously if an error occurs
	      	    // or server returns response with an error status.
	      	  });
	      };
	    }

});

app.directive('rut', ['$filter', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function ($scope, $element, $attr, $ctrl) {
            function customValidator(ngModelValue) {
                if(ngModelValue.length == 0)
                {
                    $ctrl.$setValidity('rutValidator', true);
                }
                else if(ngModelValue.length < 9)
                {
                    $ctrl.$setValidity('rutValidator', false);
                }
                else
                {
                    ngModelValue = ngModelValue.replace(/\./g, '');
                    ngModelValue = ngModelValue.replace(/\-/g, '');
                    if(ngModelValue.match(/^[^0-9]$/))
                    {
                        $ctrl.$setValidity('rutValidator', false);
                    }
                    else
                    {
                        if(ngModelValue.length > 12 || ngModelValue.length < 8)
                        {
                            $ctrl.$setValidity('rutValidator', false);
                        }
                        else
                        {
                            numero = ngModelValue.substring(0, ngModelValue.length - 1);
                            var verificador = ngModelValue.substring(ngModelValue.length -1, ngModelValue.length);

                            total = 0;
                            factor = 2;

                            for (var i = numero.length; i >= 1; i--) {
                                total += parseInt(numero.substring(i - 1, i)) * factor;
                                factor++;

                                if(factor == 8)
                                {
                                    factor = 2;
                                }
                            }    

                            resto = total%11;
                            ver = 11 - resto;
                            if(ver == 11)
                            {
                                ver = 0;
                            }
                            if(ver == 10)
                            {
                                ver = 'K';
                            }

                            if(ver == verificador.toUpperCase())
                            {
                                $ctrl.$setValidity('rutValidator', true);
                                $element.val((numero.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + '-' + verificador).toUpperCase());
                            }
                            else
                            {
                                $ctrl.$setValidity('rutValidator', false);
                            }
                        }
                    }
                }
                return ngModelValue;
            }
            $ctrl.$parsers.push(customValidator);
        }
    };
}]);

app.filter('rutFilter', function() {

  // In the return function, we must pass in a single parameter which will be the data we will work on.
  // We have the ability to support multiple other parameters that can be passed into the filter optionally
  return function(input) {

    input = input + '';
    var output = input.substring(0, input.length - 7) +
     '.' + input.substring(input.length - 7, input.length - 4) +
      '.' + input.substring(input.length - 4, input.length - 1) +
       '-' + input.substring(input.length - 1, input.length);

    // Do filter work here

    return output;

  }

});


app.directive('focusOn', function() {
   return function(scope, elem, attr) {
      scope.$on('focusOn', function(e, name) {
        if(name === attr.focusOn) {
          elem[0].focus();
        }
      });
   };
});

app.factory('focus', function ($rootScope, $timeout) {
  return function(name) {
    $timeout(function (){
      $rootScope.$broadcast('focusOn', name);
    });
  }
});