<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{	
	protected $table = 'regiones';

	protected $dates = [
		'created_at', 
		'updated_at', 
		'deleted_at'
	];

	protected $casts = [
	    'nombre' 	=> 'string',
	    'numero' 	=> 'integer',
	];

	protected $fillable = [
		'nombre',
		'numero',
	];

    public function provincias()
    {
        return $this->hasMany('App\Provincia');
    }

    public function ciudades()
    {
        return $this->hasMany('App\Ciudad');
    }
}
