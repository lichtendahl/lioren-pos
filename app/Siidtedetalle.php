<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siidtedetalle extends Model
{
	protected $table = 'siidtedetalles';

	protected $dates = [
		'created_at', 
		'updated_at', 
		'deleted_at'
	];

	protected $casts = [
		'siidte_id' 				=> 'integer',
		'codigo'      				=> 'string',
		'nombre' 					=> 'string',
		'descripcion' 				=> 'string',
		'cantidad' 					=> 'double',
		'unidad' 					=> 'string',
		'precio' 					=> 'integer',
		'siiimpuestoadicional_id' 	=> 'integer',
		'porcentajedescuento' 		=> 'integer',
		'exento'                	=> 'boolean',
	];

	protected $fillable = [
		'siidte_id',
		'codigo',
		'nombre',
		'descripcion',
		'cantidad',
		'unidad',
		'precio',
		'siiimpuestoadicional_id',
		'porcentajedescuento',
		'exento',
	];

	public function siidte()
	{
		return $this->belongsTo('App\Siidte');
	}

	public function siiimpuestoadicional()
	{
		return $this->belongsTo('App\Siiimpuestoadicional');
	}
}
