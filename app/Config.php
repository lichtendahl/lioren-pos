<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
	protected $table = 'config';

	protected $dates = ['created_at', 'updated_at'];

	protected $casts = [
		'client_id'      	=> 'integer',
		'token_type' 	    => 'string',
		'access_token'      => 'string',
		'refresh_token'     => 'string',
		'expires_in'     	=> 'string',
		'printer'     		=> 'string',
		'secret_local'     	=> 'string',
		'secret_remote'     => 'string',
		'sync'      		=> 'datetime',
		'fantasia'			=> 'string',
		'rut'				=> 'string',
		'fecharesolucion'				=> 'string',
		'numeroresolucion'				=> 'string',
		'logo'				=> 'string'
	];

	protected $fillable = [
		'client_id',
		'token_type',
		'access_token',
		'refresh_token',
		'expires_in',
		'printer',
		'secret_local',
		'secret_remote',
		'sync',
		'fantasia',
		'rut',
		'fecharesolucion',
		'numeroresolucion',
		'logo',
	];
}
