<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bodega extends Model
{
	protected $table = 'bodegas';

	protected $dates = ['created_at', 'updated_at'];

	protected $casts = [
	    'nombre' 		=> 'string',
	];

	protected $fillable = [
		'nombre',
	];

    public function sucursal()
    {
        return $this->belongsTo('App\Sucursal');
    }
}