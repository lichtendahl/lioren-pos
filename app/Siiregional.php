<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siiregional extends Model
{
    protected $table = 'siiregionales';

    protected $dates = [
        'created_at', 
        'updated_at', 
        'deleted_at'
    ];

    protected $casts = [
        'nombre' 	=> 'string',
        'activo'    => 'boolean',
    ];

    protected $fillable = [
        'nombre',
        'activo',
    ];

    public function comunas()
    {
        return $this->hasMany('App\Comuna');
    }
}
