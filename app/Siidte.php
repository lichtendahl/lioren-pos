<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siidte extends Model
{
	protected $table = 'siidtes';

	protected $dates = [
		'created_at', 
		'updated_at', 
	];

	protected $casts = [
		'user_id'               => 'integer',
		'rscontribuyente'       => 'string',
		'comunaorigen_id'       => 'integer',
		'ciudadorigen_id'       => 'integer',
		'direccionorigen'       => 'string',
		'localidadorigen'       => 'string',
		'siiactividad_id' 		=> 'integer',
		'siitipotraslado_id'    => 'integer',
		'siitipodespacho_id'    => 'integer',
		'telefonoemisor'    	=> 'string',
		'emailemisor'    		=> 'string',

		'sucursalnombre'        => 'string',
		'sucursalcodigosii'     => 'string',
		'sucursalcomuna'        => 'string',
		'sucursalciudad'        => 'string',
		'sucursaldireccion'     => 'string',
		'sucursallocalidad'     => 'string',

		'siitipodte_id'      	=> 'integer',
		'fecha'                 => 'date',
		'indservicio'           => 'integer',

		'rut'                   => 'string',
		'telefono'              => 'string',
		'rs'                 	=> 'string',
		'direccion'             => 'string',
		'comuna_id'             => 'integer',
		'ciudad_id'             => 'integer',
		'localidad'             => 'string',
		'giro'                  => 'string',
		'contacto'              => 'string',
		'emailcontacto'         => 'string',
		'rutpersona'            => 'string',

		'montoneto'                  => 'integer',
		'montoexento'                => 'integer',
		'montonofact'                => 'integer',
		'montoretencion'     => 'integer',
		'montoimpuestoadicional'     => 'integer',
		'pdescuentoafectos'   => 'double',
		'pdescuentoexentos'   => 'double',
		'porcentajeimpuesto'    => 'double',

		'nfolio'					=> 'integer',

		'bodega_id'     	=> 'integer',

		'ted'     	=> 'string',
	];

	protected $fillable = [
		'user_id',
		'rscontribuyente',
		'comunaorigen_id',
		'ciudadorigen_id',
		'direccionorigen',
		'localidadorigen',
		'siiactividad_id',
		'siitipotraslado_id',
		'siitipodespacho_id',
		'telefonoemisor',
		'emailemisor',

		'sucursalnombre' ,
		'sucursalcodigosii',
		'sucursalcomuna',
		'sucursalciudad',
		'sucursaldireccion',
		'sucursallocalidad',

		'siitipodte_id',
		'fecha',
		'indservicio',

		'rut',
		'telefono',
		'rs',
		'direccion',
		'comuna_id',
		'ciudad_id',
		'localidad',
		'giro',
		'contacto',
		'emailcontacto',
		'rutpersona',

		'montoneto',
		'montoexento',
		'montonofact',
		'montoretencion',
		'montoimpuestoadicional',
		'pdescuentoafectos',
		'pdescuentoexentos',
		'porcentajeimpuesto',

		'nfolio',

		'bodega_id',

		'ted',
	];

	public function siitipodte()
	{
		return $this->belongsTo('App\Siitipodte');
	}

	public function siiactividad()
	{
		return $this->belongsTo('App\Siiactividad');
	}

	public function siidtedetalles()
	{
		return $this->hasMany('App\Siidtedetalle');
	}

	public function siidteinfopagos()
	{
		return $this->hasMany('App\Siidteinfopago');
	}

	public function siidtereferencias()
	{
		return $this->hasMany('App\Siidtereferencia');
	}

	public function comuna()
	{
		return $this->belongsTo('App\Comuna');
	}

	public function comunaorigen()
	{
		return $this->belongsTo('App\Comuna', 'comunaorigen_id');
	}

	public function ciudad()
	{
		return $this->belongsTo('App\Ciudad');
	}

	public function ciudadorigen()
	{
		return $this->belongsTo('App\Ciudad', 'ciudadorigen_id');
	}

	public function bodega()
	{
		return $this->belongsTo('App\Bodega');
	}
}
