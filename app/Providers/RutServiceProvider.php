<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;
use App\User;

class RutServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('rutchile', function($attribute, $value, $parameters, $validator) {
            $value = strtoupper(preg_replace('/[.,-]*/', '', $value));

            if(strlen($value) == 0)
            {
                return true;
            }
            if(strlen($value) < 8 || strlen($value) > 10)
            {
                return false;
            }
            else
            {
                if(!preg_match('/[1-9]{1}[0-9]{6,7}[0-9k]{1}/is', $value))
                {
                    return false;
                }
                $numero = substr($value, 0, strlen($value) - 1);
                $verificador = substr($value, strlen($value) - 1, 1);

                $total = 0;
                $factor = 2;

                for ($i = strlen($numero); $i >= 1; $i--) {
                    $total += intval(substr($numero, $i -1, 1)) * $factor;
                    $factor++;

                    if($factor == 8)
                    {
                        $factor = 2;
                    }
                }    

                $resto = $total % 11;
                $ver = 11 - $resto;
                if($ver == 11)
                {
                    $ver = '0';
                }
                if($ver == 10)
                {
                    $ver = 'K';
                }

                if((string)$ver ==  strtoupper(((string)$verificador)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
