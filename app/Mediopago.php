<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mediopago extends Model
{
    protected $table = 'mediopagos';

    protected $dates = [
        'created_at', 
        'updated_at', 
    ];

    protected $casts = [
        'nombre' => 'string',
    ];

    protected $fillable = [
    	'nombre',
    ];
}
