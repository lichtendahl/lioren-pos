<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siiactividad extends Model
{
	protected $table = 'siiactividades';

	protected $dates = [
	    'created_at', 
	    'updated_at',
	];

	protected $casts = [
	    'codigo' 	=> 'integer',
	    'nombre' 	=> 'string',
	    'categoria' => 'integer',
	    'iva' 		=> 'integer',
	];

	protected $fillable = [
		'codigo',
		'nombre',
		'categoria',
		'iva',
	];
}
