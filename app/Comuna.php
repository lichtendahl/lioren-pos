<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comuna extends Model
{	
	protected $table = 'comunas';

	protected $dates = [
		'created_at', 
		'updated_at', 
	];

	protected $casts = [
	    'nombre' 		=> 'string',
	    'numero' 		=> 'integer',
	    'provincia_id'	=> 'integer',
	    'siiregional_id'	=> 'integer',
	];

	protected $fillable = [
		'nombre',
		'numero',
		'provincia_id',
		'siiregional_id',
	];

	public function provincia()
	{
	    return $this->belongsTo('App\Provincia');
	}

    public function siiregional()
    {
        return $this->belongsTo('App\Siiregional');
    }
}
