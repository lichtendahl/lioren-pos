<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    protected $table = 'unidades';

    protected $dates = ['created_at', 'updated_at'];

    protected $casts = [
        'nombre' => 'string',
        'abreviacion' => 'string',
        'tipo' => 'string',
    ];

    protected $fillable = [
        'nombre',
        'abreviacion',
        'tipo',
    ];

    public function productos()
    {
    	return $this->hasMany('App\Producto');
    }
}
