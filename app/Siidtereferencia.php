<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siidtereferencia extends Model
{
	protected $table = 'siidtereferencias';

	protected $dates = [
		'created_at', 
		'updated_at', 
	];

	protected $casts = [
		'siidte_id'             => 'integer',
		'siitipodte_id' 		=> 'integer',
		'siirazonreferencia_id' => 'integer',
		'glosa'					=> 'string',
		'global'				=> 'boolean',
		'folio'					=> 'integer',
		'fecha'					=> 'date',
	];

	protected $fillable = [
		'siidte_id',
		'siitipodte_id',
		'siirazonreferencia_id',
		'glosa',
		'global',
		'folio',
		'fecha',
	];

	public function siidte()
	{
		return $this->belongsTo('App\Siidte');
	}

	public function siitipodte()
	{
		return $this->belongsTo('App\Siitipodte');
	}

	public function siirazonreferencia()
	{
		return $this->belongsTo('App\Siirazonreferencia');
	}
}
