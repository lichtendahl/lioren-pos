<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoriaproducto extends Model
{
	protected $table = 'categoriaproductos';

	protected $dates = ['created_at', 'updated_at'];

	protected $casts = [
	    'param1' 		=> 'string',
	    'param2' 		=> 'string',
	    'param3' 		=> 'string',
	];

	protected $fillable = [
	    'param1',
	    'param2',
	    'param3',
	];

	public function empresa()
	{
		return $this->belongsTo('App\Empresa');
	}
}
