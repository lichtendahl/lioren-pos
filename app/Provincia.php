<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{	
	protected $table = 'provincias';

	protected $dates = [
		'created_at', 
		'updated_at', 
	];

	protected $casts = [
	    'nombre' 	=> 'string',
	    'numero' 	=> 'integer',
	    'region_id'	=> 'integer',
	];

	protected $fillable = [
		'nombre',
		'numero',
		'region_id',
	];

    public function comunas()
    {
        return $this->hasMany('App\Comuna');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }
}
