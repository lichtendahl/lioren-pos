<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puntocaja extends Model
{
	protected $table = 'puntocajas';

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	protected $casts = [
	    'user_id' 		=> 'integer',
	    'abierta'		=> 'boolean',
	    'activa'		=> 'boolean',

	    'horaapertura'	=> 'datetime',
	    'am1'			=> 'integer',
	    'am5'			=> 'integer',
	    'am10'			=> 'integer',
	    'am50'			=> 'integer',
	    'am100'			=> 'integer',
	    'am500'			=> 'integer',
	    'ab1000'		=> 'integer',
	    'ab2000'		=> 'integer',
	    'ab5000'		=> 'integer',
	    'ab10000'		=> 'integer',
	    'ab20000'		=> 'integer',
	    'amonto'		=> 'integer',
	    'aobservaciones'=> 'string',

	    'horacierre'	=> 'datetime',
	    'cm1'			=> 'integer',
	    'cm5'			=> 'integer',
	    'cm10'			=> 'integer',
	    'cm50'			=> 'integer',
	    'cm100'			=> 'integer',
	    'cm500'			=> 'integer',
	    'cb1000'		=> 'integer',
	    'cb2000'		=> 'integer',
	    'cb5000'		=> 'integer',
	    'cb10000'		=> 'integer',
	    'cb20000'		=> 'integer',
	    'cmonto'		=> 'integer',
	    'cobservaciones'=> 'string',

	    'closer_id'		=>'integer',
	];

	protected $fillable = [
		'user_id',
		'abierta',
		'activa',

		'horaapertura',
		'am1',
		'am5',
		'am10',
		'am50',
		'am100',
		'am500',
		'ab1000',
		'ab2000',
		'ab5000',
		'ab10000',
		'ab20000',
		'amonto',
		'aobservaciones',

		'horacierre',
		'cm1',
		'cm5',
		'cm10',
		'cm50',
		'cm100',
		'cm500',
		'cb1000',
		'cb2000',
		'cb5000',
		'cb10000',
		'cb20000',
		'cmonto',
		'cobservaciones',

		'closer_id',
	];

    public function punto()
    {
        return $this->belongsTo('App\Punto');
    }
    
    public function vendedor()
    {
        return $this->belongsTo('App\Vendedor');
    }

    public function dte()
    {
    	return $this->belongsTo('App\Dte');
    }
}
