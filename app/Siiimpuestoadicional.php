<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siiimpuestoadicional extends Model
{
	protected $table = 'siiimpuestoadicionales';

	protected $dates = [
		'created_at', 
		'updated_at', 
	];

	protected $casts = [
		'codigosii'   => 'integer',
		'porcentaje'  => 'double',
		'nombre' 	  => 'string',
		'retencion' => 'double',
	];

	protected $fillable = [
		'codigosii',
		'porcentaje',
		'nombre',
		'retencion',
	];
}
