<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siidteinfopago extends Model
{
    protected $table = 'siidteinfopagos';

    protected $dates = [
        'created_at', 
        'updated_at', 
        'deleted_at'
    ];

    protected $casts = [
        'siidte_id' 	=> 'integer',
        'fecha'         => 'date',
        'monto'      	=> 'integer',
        'glosa'         => 'string',
        'mediopago_id'  => 'integer',
    ];

    protected $fillable = [
        'siidte_id',
        'fecha',
        'monto',
        'glosa',
        'mediopago_id',
    ];

    public function siidte()
    {
        return $this->belongsTo('App\Siidte');
    }

    public function mediopago()
    {
        return $this->belongsTo('App\Mediopago');
    }
}
