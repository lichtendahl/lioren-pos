<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class PrinterController extends Controller
{
    use PrintTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function run(Request $request)
    {
        if(isset($request->data))
        {
            return response()->json($this->printArray($request->data));
        }
        return response()->json(FALSE);
    }

    public function test(Request $request)
    {
        $this->printTest();

        return response()->json(TRUE);
    }
}
