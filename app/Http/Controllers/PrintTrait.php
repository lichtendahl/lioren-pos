<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Storage;
use Auth;

use App\Siidte;
use App\Config;
use App\User;

require __DIR__ . '/escpos-php/autoload.php';

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;

trait PrintTrait
{
	public function getUID()
	{
	    return hash('sha256', env('APP_KEY'));
	}
	
	public function printSiidte(Siidte $siidte, Config $config, User $user)
	{
		try {
		    // Enter the share name for your USB printer here
		    $connector = new WindowsPrintConnector($config->printer);
		    $printer = new Printer($connector);

	        $printer -> setJustification(Printer::JUSTIFY_CENTER);
	        $printer -> setEmphasis(true);
	        $printer -> text($siidte->rscontribuyente."\n");
	        $printer -> setEmphasis(false);
	        $printer -> text("RUT ".number_format(intval(substr($config->rut, 0,-1)),0,',','.').'-'.substr($config->rut, -1)."\n");
	        $printer -> text($siidte->direccionorigen.'. '.$siidte->comunaorigen->nombre."\n");
	        $printer -> text("Emision: " . Carbon::now()->format('Y-m-d H:i:s') . "\n\n");
	        if($siidte->siitipodte->codigosii == 33)
	        {
	        	$printer -> text("Factura Electrónica N: " . $siidte->nfolio . "\n\n");
	        	$printer -> text("---------------\n");
	        	$printer -> text("RECEPTOR \n\n");
	        	$printer -> setJustification(Printer::JUSTIFY_LEFT);
	        	$printer -> text("RUT ".number_format(intval(substr($siidte->rut, 0,-1)),0,',','.').'-'.substr($siidte->rut, -1)."\n");
	        	$printer -> text($siidte->rs."\n");
	        	$printer -> text("Giro: ".$siidte->giro."\n");
	        	$printer -> text("Direccion ".$siidte->direccion.'. '.$siidte->comuna->nombre."\n");
	        	$printer -> text("Solicita: ".$siidte->rutpersona."\n\n");
	        	$printer -> setJustification(Printer::JUSTIFY_CENTER);
	        }
	        $printer -> text("---------------\n");
	        $printer -> setJustification(Printer::JUSTIFY_LEFT);
	        $printer -> text("Vendedor: " . $user->id . "\n");
	        $printer -> text("Pago con: " . $siidte->siidteinfopagos->first()->mediopago->nombre . "\n");
	        $printer -> setJustification(Printer::JUSTIFY_CENTER);
	        $printer -> text("---------------\n");
	        $printer -> text("DETALLE \n");
	        $printer -> text("---------------\n");
	        $printer -> setJustification(Printer::JUSTIFY_LEFT);

	        $total = 0;
	        if($siidte->siitipodte->codigosii == 33)
	        {
	        	foreach ($siidte->siidtedetalles as $key => $value) {
	        	    $printer -> setJustification(Printer::JUSTIFY_LEFT);
	        	    $printer -> text($value->nombre . "\n");
	        	    $printer -> setJustification(Printer::JUSTIFY_RIGHT);
	        	    $printer -> text('$' . number_format($value->precio, 0, ',', '.') . 
	        	        " x " . number_format($value->cantidad,0, ',', '.') . " = $" . 
	        	        number_format((intval($value->precio) * intval($value->cantidad)), 0, ',', '.') . 
	        	        "\n");
	        	}

	        	$printer -> text("\nIVA: $" . number_format(round($siidte->montoneto * 0.19), 0, ',', '.') . "\n");
	        }
	        else
	        {
	        	foreach ($siidte->siidtedetalles as $key => $value) {
	        		$precio = ($value->exento)?$value->precio:$value->precio*1.19;
	        	    $printer -> setJustification(Printer::JUSTIFY_LEFT);
	        	    $printer -> text($value->nombre . "\n");
	        	    $printer -> setJustification(Printer::JUSTIFY_RIGHT);
	        	    $printer -> text('$' . number_format($precio, 0, ',', '.') . 
	        	        " x " . number_format($value->cantidad,0, ',', '.') . " = $" . 
	        	        number_format((intval($precio) * intval($value->cantidad)), 0, ',', '.') . 
	        	        "\n");
	        	}
	        }

            $printer -> text("\nTotal de la venta: $" . number_format($siidte->montoneto + $siidte->montoexento + round($siidte->montoneto * 0.19) + $siidte->montoimpuestoadicional, 0, ',', '.') . "\n");

	        $printer -> setJustification(Printer::JUSTIFY_CENTER);
	        $printer -> text("\n----------------------\n");
	         if($siidte->siitipodte->codigosii == 33)
	         {
	         	$printer -> text("\nRes.".$config->numeroresolucion." de ".$config->fecharesolucion."\n\n\n");
	        	}
	        $printer -> cut();

		    $printer -> cut();
		    $printer -> pulse(0, 120, 240);
		    // Close printer 
		    $printer -> close();
		} catch(Exception $e) {
			return FALSE;
		}

		return TRUE;
	}

	public function abrirCaja()
	{
		$connector = new WindowsPrintConnector($config->printer);
		$printer = new Printer($connector);
		$printer -> pulse(0, 120, 240);
		$printer -> close();
	}

	public function printTest()
	{
		$connector = new WindowsPrintConnector('POS58');
		$printer = new Printer($connector);

		/* Initialize */
		$printer -> initialize();
		/* Text */
		$printer -> text("Hello world\n");
		$printer -> cut();

		/* Graphics - this demo will not work on some non-Epson printers */
		try {
		    $logo = EscposImage::load(storage_path()."/resources/escpos-php.png", false);
		    $imgModes = array(
		        Printer::IMG_DEFAULT,
		    );
		    foreach ($imgModes as $mode) {
		        $printer -> bitImageColumnFormat($logo, $mode);
		    }
		} catch (Exception $e) {
		    /* Images not supported on your PHP, or image file not found */
		    $printer -> text($e -> getMessage() . "\n");
		}
		$printer -> cut();
		/* Bit image */
		try {
		    $logo = EscposImage::load(storage_path()."/resources/escpos-php.png", false);
		    $imgModes = array(
		        Printer::IMG_DEFAULT,
		    );
		    foreach ($imgModes as $mode) {
		        $printer -> bitImageColumnFormat($logo, $mode);
		    }
		} catch (Exception $e) {
		    /* Images not supported on your PHP, or image file not found */
		    $printer -> text($e -> getMessage() . "\n");
		}
		$printer -> cut();
		/* Pulse */
		$printer -> pulse();
		/* Always close the printer! On some PrintConnectors, no actual
		 * data is sent until the printer is closed. */
		$printer -> close();
	}
	public function printTest2()
	{
		$connector = new WindowsPrintConnector('POS58');
		$printer = new Printer($connector);

		/* Initialize */
		$printer -> initialize();
		/* Text */
		$printer -> text("Hello world\n");
		$printer -> cut();
		/* Line feeds */
		$printer -> text("ABC");
		$printer -> feed(7);
		$printer -> text("DEF");
		$printer -> feedReverse(3);
		$printer -> text("GHI");
		$printer -> feed();
		$printer -> cut();
		/* Font modes */
		$modes = array(
		    Printer::MODE_FONT_B,
		    Printer::MODE_EMPHASIZED,
		    Printer::MODE_DOUBLE_HEIGHT,
		    Printer::MODE_DOUBLE_WIDTH,
		    Printer::MODE_UNDERLINE);
		for ($i = 0; $i < pow(2, count($modes)); $i++) {
		    $bits = str_pad(decbin($i), count($modes), "0", STR_PAD_LEFT);
		    $mode = 0;
		    for ($j = 0; $j < strlen($bits); $j++) {
		        if (substr($bits, $j, 1) == "1") {
		            $mode |= $modes[$j];
		        }
		    }
		    $printer -> selectPrintMode($mode);
		    $printer -> text("ABCDEFGHIJabcdefghijk\n");
		}
		$printer -> selectPrintMode(); // Reset
		$printer -> cut();
		/* Underline */
		for ($i = 0; $i < 3; $i++) {
		    $printer -> setUnderline($i);
		    $printer -> text("The quick brown fox jumps over the lazy dog\n");
		}
		$printer -> setUnderline(0); // Reset
		$printer -> cut();
		/* Cuts */
		$printer -> text("Partial cut\n(not available on all printers)\n");
		$printer -> cut(Printer::CUT_PARTIAL);
		$printer -> text("Full cut\n");
		$printer -> cut(Printer::CUT_FULL);
		/* Emphasis */
		for ($i = 0; $i < 2; $i++) {
		    $printer -> setEmphasis($i == 1);
		    $printer -> text("The quick brown fox jumps over the lazy dog\n");
		}
		$printer -> setEmphasis(false); // Reset
		$printer -> cut();
		/* Double-strike (looks basically the same as emphasis) */
		for ($i = 0; $i < 2; $i++) {
		    $printer -> setDoubleStrike($i == 1);
		    $printer -> text("The quick brown fox jumps over the lazy dog\n");
		}
		$printer -> setDoubleStrike(false);
		$printer -> cut();
		/* Fonts (many printers do not have a 'Font C') */
		$fonts = array(
		    Printer::FONT_A,
		    Printer::FONT_B,
		    Printer::FONT_C);
		for ($i = 0; $i < count($fonts); $i++) {
		    $printer -> setFont($fonts[$i]);
		    $printer -> text("The quick brown fox jumps over the lazy dog\n");
		}
		$printer -> setFont(); // Reset
		$printer -> cut();
		/* Justification */
		$justification = array(
		    Printer::JUSTIFY_LEFT,
		    Printer::JUSTIFY_CENTER,
		    Printer::JUSTIFY_RIGHT);
		for ($i = 0; $i < count($justification); $i++) {
		    $printer -> setJustification($justification[$i]);
		    $printer -> text("A man a plan a canal panama\n");
		}
		$printer -> setJustification(); // Reset
		$printer -> cut();
		/* Barcodes - see barcode.php for more detail */
		$printer -> setBarcodeHeight(80);
		$printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);
		$printer -> barcode("9876");
		$printer -> feed();
		$printer -> cut();
		/* Graphics - this demo will not work on some non-Epson printers */
		try {
		    $logo = EscposImage::load(storage_path()."/resources/escpos-php.png", false);
		    $imgModes = array(
		        Printer::IMG_DEFAULT,
		        Printer::IMG_DOUBLE_WIDTH,
		        Printer::IMG_DOUBLE_HEIGHT,
		        Printer::IMG_DOUBLE_WIDTH | Printer::IMG_DOUBLE_HEIGHT
		    );
		    foreach ($imgModes as $mode) {
		        $printer -> graphics($logo, $mode);
		    }
		} catch (Exception $e) {
		    /* Images not supported on your PHP, or image file not found */
		    $printer -> text($e -> getMessage() . "\n");
		}
		$printer -> cut();
		/* Bit image */
		try {
		    $logo = EscposImage::load(storage_path()."/resources/escpos-php.png", false);
		    $imgModes = array(
		        Printer::IMG_DEFAULT,
		        Printer::IMG_DOUBLE_WIDTH,
		        Printer::IMG_DOUBLE_HEIGHT,
		        Printer::IMG_DOUBLE_WIDTH | Printer::IMG_DOUBLE_HEIGHT
		    );
		    foreach ($imgModes as $mode) {
		        $printer -> bitImage($logo, $mode);
		    }
		} catch (Exception $e) {
		    /* Images not supported on your PHP, or image file not found */
		    $printer -> text($e -> getMessage() . "\n");
		}
		$printer -> cut();
		/* QR Code - see also the more in-depth demo at qr-code.php */
		$testStr = "Testing 123";
		$models = array(
		    Printer::QR_MODEL_1 => "QR Model 1",
		    Printer::QR_MODEL_2 => "QR Model 2 (default)",
		    Printer::QR_MICRO => "Micro QR code\n(not supported on all printers)");
		foreach ($models as $model => $name) {
		    $printer -> qrCode($testStr, Printer::QR_ECLEVEL_L, 3, $model);
		    $printer -> text("$name\n");
		    $printer -> feed();
		}
		$printer -> cut();
		/* Pulse */
		$printer -> pulse();
		/* Always close the printer! On some PrintConnectors, no actual
		 * data is sent until the printer is closed. */
		$printer -> close();
	}

	public function printArray($data)
	{
		$uid = $this->getUID();
		if($data[1]['secret'] != $uid)
		{
			return FALSE;
		}

		try {

			$connector = new WindowsPrintConnector($data[0]['printer']);
			$printer = new Printer($connector);

			array_shift($data);
			array_shift($data);

			foreach ($data as $key => $value) {
				switch ($value['type']) {
					case 'barcode':
						$printer -> barcode($value['content'], $value['barcodetype']);
						break;

					case 'bitImageColumnFormat':
						Storage::put('temp.png', base64_decode($value['image']));
						$imagePath = EscposImage::load(storage_path('app/temp.png'));
						switch ($value['size']) {
							case 'IMG_DEFAULT':
								$printer -> bitImageColumnFormat($imagePath, Printer::IMG_DEFAULT);
								break;

							case 'IMG_DOUBLE_WIDTH':
								$printer -> bitImageColumnFormat($imagePath, Printer::IMG_DOUBLE_WIDTH);
								break;
								
							case 'IMG_DOUBLE_HEIGHT':
								$printer -> bitImageColumnFormat($imagePath, Printer::IMG_DOUBLE_HEIGHT);
								break;
							
							default:
								# code...
								break;
						}

						break;

					case 'bitImage':
						Storage::put('temp.bmp', base64_decode($value['image']));
						$imagePath = EscposImage::load(storage_path('app/temp.bmp'));
						switch ($value['size']) {
							case 'IMG_DEFAULT':
								$printer -> bitImage($imagePath, Printer::IMG_DEFAULT);
								break;

							case 'IMG_DOUBLE_WIDTH':
								$printer -> bitImage($imagePath, Printer::IMG_DOUBLE_WIDTH);
								break;
								
							case 'IMG_DOUBLE_HEIGHT':
								$printer -> bitImage($imagePath, Printer::IMG_DOUBLE_HEIGHT);
								break;
							
							default:
								# code...
								break;
						}

						break;

					case 'cut':
						switch ($value['mode']) {
							case 'CUT_FULL':
								$printer -> cut(Printer::CUT_FULL, $value['lines']);
								break;
								
							case 'CUT_PARTIAL':
								$printer -> cut(Printer::CUT_PARTIAL, $value['lines']);
								break;
							
							default:
								# code...
								break;
						}
						break;

					case 'feed':
						$printer -> feed($value['lines']);
						break;

					case 'feedForm':
						$printer -> feedForm();
						break;

					case 'feedReverse':
						$printer -> feedReverse($value['lines']);
						break;

					case 'graphics':
						Storage::put('temp.png', base64_decode($value['image']));
						$imagePath = EscposImage::load(storage_path('app/temp.png'));
						switch ($value['size']) {
							case 'IMG_DEFAULT':
								$printer -> graphics($imagePath, Printer::IMG_DEFAULT);
								break;

							case 'IMG_DOUBLE_WIDTH':
								$printer -> graphics($imagePath, Printer::IMG_DOUBLE_WIDTH);
								break;
								
							case 'IMG_DOUBLE_HEIGHT':
								$printer -> graphics($imagePath, Printer::IMG_DOUBLE_HEIGHT);
								break;
							
							default:
								# code...
								break;
						}
						break;

					case 'initialize':
						$printer -> initialize();
						break;

					case 'pdf417Code':
						switch ($value['options']) {
							case 'PDF417_STANDARD':
								$printer -> pdf417Code($value['content'], $value['width'], $value['heightMultiplier'], $value['dataColumnCount'], $value['ec'], Printer::PDF417_STANDARD);
								break;

							case 'PDF417_TRUNCATED':
								$printer -> pdf417Code($value['content'], $value['width'], $value['heightMultiplier'], $value['dataColumnCount'], $value['ec'], Printer::PDF417_TRUNCATED);
								break;
							
							default:
								# code...
								break;
						}
						break;

					case 'pulse':
						$printer -> pulse($value['pin'], $value['on_ms'], $value['off_ms']);
						break;

					case 'qrCode':
						switch ($value['model']) {
							case 'QR_MODEL_1':
								$model = Printer::QR_MODEL_1;
								break;
								
							case 'QR_MODEL_2':
								$model = Printer::QR_MODEL_2;
								break;

							case 'QR_MICRO':
								$model = Printer::QR_MICRO;
								break;
							
							default:
								return false;
								break;
						}

						switch ($value['ec']) {
							case 'QR_ECLEVEL_L':
								$printer -> qrCode($value['content'], Printer::QR_ECLEVEL_L, $value['size'], $model);
								break;
								
							case 'QR_ECLEVEL_M':
								$printer -> qrCode($value['content'], Printer::QR_ECLEVEL_M, $value['size'], $model);
								break;
								
							case 'QR_ECLEVEL_Q':
								$printer -> qrCode($value['content'], Printer::QR_ECLEVEL_Q, $value['size'], $model);
								break;

							case 'QR_ECLEVEL_H':
								$printer -> qrCode($value['content'], Printer::QR_ECLEVEL_H, $value['size'], $model);
								break;
							
							default:
								# code...
								break;
						}
						break;

					case 'selectPrintMode':
						switch ($value['mode']) {
							case 'MODE_FONT_A':
								$printer -> selectPrintMode(Printer::MODE_FONT_A);
								break;

							case 'MODE_FONT_B':
								$printer -> selectPrintMode(Printer::MODE_FONT_B);
								break;

							case 'MODE_EMPHASIZED':
								$printer -> selectPrintMode(Printer::MODE_EMPHASIZED);
								break;

							case 'MODE_DOUBLE_HEIGHT':
								$printer -> selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
								break;

							case 'MODE_DOUBLE_WIDTH':
								$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
								break;

							case 'MODE_UNDERLINE':
								$printer -> selectPrintMode(Printer::MODE_UNDERLINE);
								break;
							
							default:
								# code...
								break;
						}
						break;

					case 'setBarcodeHeight':
						$printer -> setBarcodeHeight($value['height']);
						break;

					case 'setBarcodeWidth':
						$printer -> setBarcodeWidth($value['width']);
						break;

					case 'setColor':
						switch ($value['color']) {
							case 'COLOR_1':
								$printer -> setColor(Printer::COLOR_1);
								break;

							case 'COLOR_2':
								$printer -> setColor(Printer::COLOR_2);
								break;
							
							default:
								# code...
								break;
						}
						break;

					case 'setDoubleStrike':
						$printer -> setDoubleStrike($value['on']);
						break;

					case 'setEmphasis':
						$printer -> setEmphasis($value['on']);
						break;

					case 'setFont':
						switch ($value['font']) {
							case 'FONT_A':
								$printer -> setFont(Printer::FONT_A);
								break;

							case 'FONT_B':
								$printer -> setFont(Printer::FONT_B);
								break;

							case 'FONT_C':
								$printer -> setFont(Printer::FONT_C);
								break;
							
							default:
								# code...
								break;
						}
						break;

					case 'setJustification':
						switch ($value['justification']) {
							case 'JUSTIFY_LEFT':
								$printer -> setJustification(Printer::JUSTIFY_LEFT);
								break;
								
							case 'JUSTIFY_CENTER':
								$printer -> setJustification(Printer::JUSTIFY_CENTER);
								break;

							case 'JUSTIFY_RIGHT':
								$printer -> setJustification(Printer::JUSTIFY_RIGHT);
								break;
							
							default:
								# code...
								break;
						}
						break;

					case 'setLineSpacing':
						$printer -> setLineSpacing($value['height']);
						break;

					case 'setPrintLeftMargin':
						$printer -> setPrintLeftMargin($value['margin']);
						break;

					case 'setPrintWidth':
						$printer -> setPrintWidth($value['width']);
						break;

					case 'setReverseColors':
						$printer -> setReverseColors($value['on']);
						break;

					case 'setTextSize':
						$printer -> setTextSize($value['widthMultiplier'], $value['heightMultiplier']);
						break;

					case 'setUnderline':
						switch ($value['underline']) {
							case 'UNDERLINE_NONE':
								$printer -> setUnderline(Printer::UNDERLINE_NONE);
								break;
								
							case 'UNDERLINE_SINGLE':
								$printer -> setUnderline(Printer::UNDERLINE_SINGLE);
								break;

							case 'UNDERLINE_DOUBLE':
								$printer -> setUnderline(Printer::UNDERLINE_DOUBLE);
								break;
							
							default:
								$printer -> setUnderline(Printer::UNDERLINE_SINGLE);
								break;
						}
						break;

					case 'text':
						$printer -> text($value['str']);
						break;
					
					default:
						# code...
						break;
				}
			}

		    $printer -> close();

		} 
		catch(Exception $e) 
		{
			return FALSE;
		}

		return TRUE;
	}
}