<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use Carbon\Carbon;

class SetupController extends Controller
{
	use UIDTrait;

    public function index()
    {
    	$uid = $this->getUID();
    	return view('setup.index', ['uid' => $uid]);
    }

    public function reset(Request $request)
    {
    	$data = json_decode($request->data, TRUE);

    	$http = new \GuzzleHttp\Client();
    	$config = Config::first();

    	$response = $http->post('http://lioren.io/api/pos/reset', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$config->access_token,
            ],
    	    'form_params' => [
    	        'email' => $data['email'],
    	        'password' => $data['password'],
    	    ],
    	]);

    	$response = json_decode((string)$response->getBody(), TRUE);

    	if ($response === TRUE) {
    		$config = Config::first();
    		$config->delete();

    		$messages = ['Sistema restaurado a su estado inicial.'];
    		return redirect(url('setup'))->with('messages', $messages);
    	}
    	else
    	{
    		$errors = ['No se ha podido verificar la autenticidad del Administrador.'];
    		return redirect(url('setup'))->withErrors($errors);
    	}
    }

    public function inicio()
    {
    	$config = Config::first();
    	if($config != NULL)
    	{
    		$config->delete();
    	}

    	return view('setup.paso1');
    }

    public function paso1test(Request $request)
    {
    	$oldconf = Config::first();
    	if($oldconf != NULL)
    	{
    		$oldconf->delete();
    	}

    	$config = Config::create([
    		'client_id' => $request->client_id,
    		'secret_remote' => $request->secret,
    		]);

    	$query = http_build_query([
    	    'client_id' => $config->client_id,
    	    'redirect_uri' => 'http://lioren.pos/auth/callback',
    	    'response_type' => 'code',
    	    'scope' => '',
    	]);

    	return redirect('http://lioren.io/oauth/authorize?'.$query);
    }

    public function paso2(Request $request)
    {
    	$config = Config::first();
    	$config->secret_local = $this->getUID();
    	$config->save();

    	$secret_local = $config->secret_local;

    	return view('setup.paso2', ['config' => $config]);
    }

    public function paso2test(Request $request)
    {
    	$http = new \GuzzleHttp\Client();
    	$config = Config::first();

        $response = $http->post('http://lioren.io/api/test', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$config->access_token,
            ],
            'form_params' => [
                'client_id' => $config->client_id,
            ],
        ]);

    	$response = json_decode((string)$response->getBody(), TRUE);

    	$validator = Validator::make($response, [
    	    'status' => 'required|string|in:ok',
    	    'sync'	 => 'required|string|date_format:Y-m-d H:i:s',
            'fantasia'  => 'required|string',
            'logo'  => 'required|string|nullable',
    	]);

    	if ($validator->fails()) {
    		$config->sync = NULL;
    		$config->save();

    	    return redirect(url('setup/2'))->withErrors($validator);
    	}
    	else
    	{
            $config->sync = Carbon::createFromFormat('Y-m-d H:i:s', $response['sync']);
            $config->fantasia = $response['fantasia'];
            $config->rut = $response['rut'];
            $config->fecharesolucion = $response['fecharesolucion'];
    		$config->numeroresolucion = $response['numeroresolucion'];
            if($response['logo'] != NULL)
            {
                $config->logo = $response['logo'];
            }
    		$config->save();

            $messages = ['Prueba de conexión confirmada.'];
            return redirect(url('setup/3'))->with('messages', $messages);
    	}
    }

    public function paso3()
    {
        return view('setup.paso3');
    }

    public function paso3test(Request $request)
    {
        $config = Config::first();
        $config->printer = $request->printer;
        $config->save();

        $this->print();

        $messages = ['Prueba de Impresión realizada'];
        return redirect(url('setup/4'))->with('messages', $messages);
    }

    public function paso4()
    {
        return view('setup.paso4');
    }

    public function callback(Request $request)
    {
	    $http = new \GuzzleHttp\Client();

	    $config = Config::first();

	    $response = $http->post('http://lioren.io/oauth/token', [
	        'form_params' => [
	            'grant_type' => 'authorization_code',
	            'client_id' => $config->client_id,
	            'client_secret' => $config->secret_remote,
	            'redirect_uri' => 'http://lioren.pos/auth/callback',
	            'code' => $request->code,
	        ],
	    ]);

	    $data = json_decode((string)$response->getBody());
	    $config->token_type = $data->token_type;
	    $config->expires_in = $data->expires_in;
	    $config->access_token = $data->access_token;
	    $config->refresh_token = $data->refresh_token;
	    $config->save();

	    return redirect(url('setup/2'))->with('messages', ['Autorizacion procesada']);
    }
}
