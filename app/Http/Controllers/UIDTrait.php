<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Storage;
use Auth;

trait UIDTrait
{
	public function getUID()
	{
	    return hash('sha256', env('APP_KEY'));
	}
}