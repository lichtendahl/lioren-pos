<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Config;
use App\Currentuser;
use App\Modulo;
use App\Permiso;
use App\User;
use App\Bodega;
use App\Region;
use App\Provincia;
use App\Siiregional;
use App\Comuna;
use App\Ciudad;
use App\Sucursal;

use App\Mediopago;
use App\Siitipodte;
use App\Siiimpuestoadicional;
use App\Siiivanofacturable;
use App\Siiactividad;

use App\Producto;
use App\Categoriaproducto;
use App\Unidad;

use DB;

class InicioController extends Controller
{
    public function index()
    {
    	$config = Config::first();
    	if($config == NULL)
    	{
    		return redirect(url('/'))->withErrors('Punto de Ventas no configurado');
    	}
    	return view('inicio.index');
    }

    public function iniciarSesion(Request $request)
    {
    	$data = json_decode($request->data, TRUE);

    	$http = new \GuzzleHttp\Client();
    	$config = Config::first();

		$response = $http->post('http://lioren.io/api/inicio/iniciarsesion', [
	        'headers' => [
	            'Accept' => 'application/json',
	            'Authorization' => 'Bearer '.$config->access_token,
	        ],
	        'form_params' => [
	        	'client_id' => $config->client_id,
	            'email' 	=> $request->email,
	            'password' 	=> $request->password,
	        ],
		]);

		$response = json_decode((string)$response->getBody(), TRUE);

		if($response['status'] === 'ok')
		{
			$oldusers = Currentuser::get();
			foreach ($oldusers as $key => $value) {
				$value->delete();
			}

			$currentuser = Currentuser::create([
				'email' 	=> $request->email,
				'password' 	=> $response['pw'],
			]);

			$user = User::where('email', $request->email)->first();
			if($user != NULL)
			{
				$user->name = $response['name'];
				$user->save();
			}
			else
			{
				$user = User::create([
					'name' => $response['name'],
					'email' => $request->email,
					]);
			}

			return response()->json(TRUE);
		}
		else
		{
			$oldusers = Currentuser::get();
			foreach ($oldusers as $key => $value) {
				$value->delete();
			}

			return response()->json(FALSE);
		}
    }

    public function getModulos()
    {
		$http = new \GuzzleHttp\Client();
		$config = Config::first();
		$currentuser = Currentuser::first();

		$response = $http->post('http://lioren.io/api/inicio/getmodulos', [
	        'headers' => [
	            'Accept' => 'application/json',
	            'Authorization' => 'Bearer '.$config->access_token,
	        ],
		    'form_params' => [
		    	'client_id' => $config->client_id,
		    	'email' 	=> $currentuser->email,
		    	'password' 	=> $currentuser->password,
		    ],
		]);

		$response = json_decode((string)$response->getBody(), TRUE);

		if($response['status'] === 'ok')
		{
			$modulos = Modulo::getQuery()->delete();

			foreach ($response['data'] as $key => $value) {
				Modulo::create([
					'numero' => $value['numero'],
				]);
			}

			return response()->json(TRUE);
		}
		else
		{
			return response()->json(FALSE);
		}
    }

    public function getPermisos()
    {
		$http = new \GuzzleHttp\Client();
		$config = Config::first();
		$currentuser = Currentuser::first();

		$response = $http->post('http://lioren.io/api/inicio/getpermisos', [
	        'headers' => [
	            'Accept' => 'application/json',
	            'Authorization' => 'Bearer '.$config->access_token,
	        ],
		    'form_params' => [
		    	'client_id' => $config->client_id,
		    	'email' 	=> $currentuser->email,
		    	'password' 	=> $currentuser->password,
		    ],
		]);

		$response = json_decode((string)$response->getBody(), TRUE);

		if($response['status'] === 'ok')
		{
			$permisos = Permiso::getQuery()->delete();

			foreach ($response['data'] as $key => $value) {
				Permiso::create([
					'numero' => $value,
				]);
			}

			return response()->json(TRUE);
		}
		else
		{
			return response()->json(FALSE);
		}
    }

    public function getRegional()
    {
		$http = new \GuzzleHttp\Client();
		$config = Config::first();
		$currentuser = Currentuser::first();

		$response = $http->post('http://lioren.io/api/inicio/getregional', [
	        'headers' => [
	            'Accept' => 'application/json',
	            'Authorization' => 'Bearer '.$config->access_token,
	        ],
		    'form_params' => [
		    	'client_id' => $config->client_id,
		    	'email' 	=> $currentuser->email,
		    	'password' 	=> $currentuser->password,
		    ],
		]);

		$response = json_decode((string)$response->getBody(), TRUE);

		$data = $response['data'];

		if($response['status'] === 'ok')
		{
			$regiones = Region::getQuery()->delete();
			foreach ($data['regiones'] as $key => $value) {
				DB::table('regiones')->insert([
					'id' => $value['id'],
					'nombre' => $value['nombre'],
					'numero' => $value['numero'],
				]);
			}

			$provincias = Provincia::getQuery()->delete();
			foreach ($data['provincias'] as $key => $value) {
				DB::table('provincias')->insert([
					'id' => $value['id'],
					'nombre' => $value['nombre'],
					'numero' => $value['numero'],
					'region_id' => $value['region_id'],
				]);
			}

			$siiregionales = Siiregional::getQuery()->delete();
			foreach ($data['siiregionales'] as $key => $value) {
				DB::table('siiregionales')->insert([
					'id' 				=> $value['id'],
					'nombre' 			=> $value['nombre'],
				]);
			}

			$comunas = Comuna::getQuery()->delete();
			foreach ($data['comunas'] as $key => $value) {
				DB::table('comunas')->insert([
					'id' 				=> $value['id'],
					'nombre' 			=> $value['nombre'],
					'numero' 			=> $value['numero'],
					'provincia_id' 		=> $value['provincia_id'],
					'siiregional_id' 	=> $value['siiregional_id'],
				]);
			}

			$ciudades = Ciudad::getQuery()->delete();
			foreach ($data['ciudades'] as $key => $value) {
				DB::table('ciudades')->insert([
					'id' 				=> $value['id'],
					'nombre' 			=> $value['nombre'],
					'region_id' 		=> $value['region_id'],
				]);
			}

			$sucursal = Sucursal::getQuery()->delete();
			if($data['sucursal'] != NULL)
			{
				DB::table('sucursales')->insert([
					'id' 		=> $data['sucursal']['id'],
					'nombre' 	=> $data['sucursal']['nombre'],
					'codigosii' => $data['sucursal']['codigosii'],
					'direccion' => $data['sucursal']['direccion'],
					'comuna_id' => $data['sucursal']['comuna_id'],
					'ciudad_id' => $data['sucursal']['ciudad_id'],
					'localidad' => $data['sucursal']['localidad'],
					'telefono' 	=> $data['sucursal']['telefono'],
					'email' 	=> $data['sucursal']['email'],
				]);
			}

			$bodegas = Bodega::getQuery()->delete();
			foreach ($data['bodegas'] as $key => $value) {
				DB::table('bodegas')->insert([
					'id' 		=> $value['id'],
					'nombre' 	=> $value['nombre'],
				]);
			}

			return response()->json(TRUE);
		}
		else
		{
			return response()->json(FALSE);
		}
    }

    public function getTributario()
    {
		$http = new \GuzzleHttp\Client();
		$config = Config::first();
		$currentuser = Currentuser::first();

		$response = $http->post('http://lioren.io/api/inicio/gettributario', [
	        'headers' => [
	            'Accept' => 'application/json',
	            'Authorization' => 'Bearer '.$config->access_token,
	        ],
		    'form_params' => [
		    	'client_id' => $config->client_id,
		    	'email' 	=> $currentuser->email,
		    	'password' 	=> $currentuser->password,
		    ],
		]);

		$response = json_decode((string)$response->getBody(), TRUE);

		$data = $response['data'];

		if($response['status'] === 'ok')
		{
			$siitipodtes = Siitipodte::getQuery()->delete();
			foreach ($data['siitipodtes'] as $key => $value) {
				DB::table('siitipodtes')->insert([
					'id' 			=> $value['id'],
					'codigosii' 	=> $value['codigosii'],
					'nombre' 		=> $value['nombre'],
					'electronico' 	=> $value['electronico'],
					'exento' 		=> $value['exento'],
				]);
			}

			$siiimpuestoadicionales = Siiimpuestoadicional::getQuery()->delete();
			foreach ($data['siiimpuestoadicionales'] as $key => $value) {
				DB::table('siiimpuestoadicionales')->insert([
					'id' 			=> $value['id'],
					'codigosii' 	=> $value['codigosii'],
					'nombre' 		=> $value['nombre'],
					'porcentaje' 	=> $value['porcentaje'],
					'retencion' 	=> $value['retencion'],
				]);
			}

			$siiivanofacturables = Siiivanofacturable::getQuery()->delete();
			foreach ($data['siiivanofacturables'] as $key => $value) {
				DB::table('siiivanofacturables')->insert([
					'id' 			=> $value['id'],
					'codigosii' 	=> $value['codigosii'],
					'nombre' 		=> $value['nombre'],
				]);
			}

			$mediopagos = Mediopago::getQuery()->delete();
			foreach ($data['mediopagos'] as $key => $value) {
				DB::table('mediopagos')->insert([
					'id' 			=> $value['id'],
					'nombre' 		=> $value['nombre'],
				]);
			}

			$siiactividades = Siiactividad::getQuery()->delete();
			foreach ($data['siiactividades'] as $key => $value) {
				DB::table('siiactividades')->insert([
					'id' 		=> $value['id'],
					'codigo' 	=> $value['codigo'],
					'nombre' 	=> $value['nombre'],
					'categoria' => $value['categoria'],
					'iva' 		=> $value['iva'],
				]);
			}

			return response()->json(TRUE);
		}
		else
		{
			return response()->json(FALSE);
		}
    }

    public function getProductos()
    {
		$http = new \GuzzleHttp\Client();
		$config = Config::first();
		$currentuser = Currentuser::first();

		$response = $http->post('http://lioren.io/api/inicio/getproductos', [
	        'headers' => [
	            'Accept' => 'application/json',
	            'Authorization' => 'Bearer '.$config->access_token,
	        ],
		    'form_params' => [
		    	'client_id' => $config->client_id,
		    	'email' 	=> $currentuser->email,
		    	'password' 	=> $currentuser->password,
		    ],
		]);

		$response = json_decode((string)$response->getBody(), TRUE);

		$data = $response['data'];

		if($response['status'] === 'ok')
		{
			$productos = Producto::getQuery()->delete();
			foreach ($data['productos'] as $key => $value) {
				DB::table('productos')->insert([
					'id'			=> $value['id'],
					'siiimpuestoadicional_id' => $value['siiimpuestoadicional_id'],
					'exento'		=> $value['exento'],
				    'codigo' 		=> $value['codigo'],
				    'nombre' 		=> $value['nombre'],
				    'unidad_id'		=> $value['unidad_id'],
				    'fraccionable'	=> $value['fraccionable'],
				    'param1' 		=> $value['param1'],
				    'param2' 		=> $value['param2'],
				    'param3' 		=> $value['param3'],
				    'descripcion' 	=> $value['descripcion'],
				    'precio'		=> $value['precio'],
				    'descuento'		=> $value['descuento'],
				    'activo'		=> $value['activo'],
				]);
			}

			$categoriaproductos = Categoriaproducto::getQuery()->delete();
			foreach ($data['categoriaproductos'] as $key => $value) {
				DB::table('categoriaproductos')->insert([
					'id' 			=> $value['id'],
					'param1' 		=> $value['param1'],
					'param2' 		=> $value['param2'],
					'param3' 		=> $value['param3'],
				]);
			}

			$unidades = Unidad::getQuery()->delete();
			foreach ($data['unidades'] as $key => $value) {
				DB::table('unidades')->insert([
					'id' 			=> $value['id'],
					'nombre' 		=> $value['nombre'],
					'abreviacion' 	=> $value['abreviacion'],
					'tipo' 			=> $value['tipo'],
				]);
			}

			return response()->json(TRUE);
		}
		else
		{
			return response()->json(FALSE);
		}
    }
}
