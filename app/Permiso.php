<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = 'permisos';

    protected $dates = ['created_at', 'updated_at'];

    protected $casts = [
    	'numero' 	=> 'integer',
    ];

    protected $fillable = [
	    'numero',
    ];
}
