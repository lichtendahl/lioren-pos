<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currentuser extends Model
{
    protected $table = 'currentuser';

    protected $dates = ['created_at', 'updated_at'];

    protected $casts = [
    	'email' 	=> 'string',
    	'password' 	=> 'string',
    ];

    protected $fillable = [
	    'email',
	    'password',
    ];
}
