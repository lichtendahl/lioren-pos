<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noseque extends Model
{
    protected $table = 'noseques';

    protected $casts = [
    	'asdasd' => 'string',
    	'imagen' => 'string',
    ];
}
