<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $table = 'modulos';

    protected $dates = ['created_at', 'updated_at'];

    protected $casts = [
    	'numero' 	=> 'integer',
    ];

    protected $fillable = [
	    'numero',
    ];
}
