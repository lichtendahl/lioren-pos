<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siitipodte extends Model
{
	protected $table = 'siitipodtes';

	protected $dates = [
		'created_at', 
		'updated_at',
	];

	protected $casts = [
		'codigosii'         => 'string',
		'nombre'            => 'string',
		'electronico'       => 'boolean',
		'exento'            => 'boolean',
	];

	protected $fillable = [
		'codigosii',
		'nombre',
		'electronico',
		'exento',
	];

}
