<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursales';

    protected $dates = ['created_at', 'updated_at'];

    protected $casts = [
        'nombre'    => 'string',
        'codigosii' => 'string',
        'direccion' => 'string',
        'comuna_id' => 'integer',
        'ciudad_id' => 'integer',
        'localidad' => 'string',
        'telefono' 	=> 'integer',
        'email'		=> 'string',
        'activo'    => 'boolean',
    ];

    protected $fillable = [
        'nombre',
        'codigosii',
	    'direccion',
	    'comuna_id',
	    'ciudad_id',
	    'localidad',
	    'telefono',
	    'email',
	    'activo',
    ];

    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function comuna()
    {
        return $this->belongsTo('App\Comuna');
    }

    public function ciudad()
    {
        return $this->belongsTo('App\Ciudad');
    }

    public function puntos()
    {
        return $this->hasMany('App\Punto');
    }
}
