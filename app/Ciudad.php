<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{    
    protected $table = 'ciudades';

    protected $dates = [
        'created_at', 
        'updated_at', 
    ];

    protected $fillable = [
    	'nombre',
    	'region_id',
    ];

    public function region()
    {
        return $this->belongsTo('App\Region');
    }
}
