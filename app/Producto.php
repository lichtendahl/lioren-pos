<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
	protected $table = 'productos';

    protected $dates = [
        'created_at', 
        'updated_at',
    ];

	protected $casts = [
		'siiimpuestoadicional_id' => 'integer',
		'exento'		=> 'boolean',
	    'codigo' 		=> 'string',
	    'nombre' 		=> 'string',
	    'unidad_id'		=> 'integer',
	    'fraccionable'	=> 'boolean',
	    'param1' 		=> 'string',
	    'param2' 		=> 'string',
	    'param3' 		=> 'string',
	    'descripcion' 	=> 'string',
	    'precio'		=> 'double',
	    'descuento'		=> 'double',
	    'activo'		=> 'boolean',
	];

	protected $fillable = [
		'siiimpuestoadicional_id',
		'exento',
	    'codigo',
	    'nombre',
	    'unidad_id',
	    'fraccionable',
	    'param1',
	    'param2',
	    'param3',
	    'descripcion',
	    'precio',
	    'descuento',
	    'activo',
	];

	public function unidad()
	{
		return $this->belongsTo('App\Unidad');
	}

	public function siiimpuestoadicional()
	{
		return $this->belongsTo('App\Siiimpuestoadicional');
	}
}
