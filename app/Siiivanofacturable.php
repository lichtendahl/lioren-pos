<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siiivanofacturable extends Model
{
	protected $table = 'siiivanofacturables';

	protected $dates = [
		'created_at', 
		'updated_at', 
	];

	protected $casts = [
		'nombre' 	=> 'string',
		'codigosii' => 'integer',
	];

	protected $fillable = [
		'nombre',
		'codigosii',
	];
}
